package Lesson1_1;

public class Main {


    public static void main(String[] args) {

        Car car = new Car();
        car.setColor("Red");
        car.setEngineType("V10");
        car.setName("Audi");
        car.setWheelDrive("OffRoad");

        System.out.println(car.getEngineType() + " " + car.getColor() + " " + car.getName());
    }
}
