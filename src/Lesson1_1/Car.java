package Lesson1_1;

public class Car {

    private String name;
    private String color;
    private String engineType;
    private String wheelDrive;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getEngineType() {
        return engineType;
    }

    public void setEngineType(String engineType) {
        if (engineType.equals("V8")) {
        this.engineType = "SuperCharge";
        return;
        }
        if(engineType.equals("V10")) {
            this.engineType = engineType;
            System.out.println("CrazyCharge");
            return;
        }
        else {
            this.engineType = engineType;
        }
    }

    public String getWheelDrive() {
        return wheelDrive;
    }

    public void setWheelDrive(String wheelDrive) {
        this.wheelDrive = wheelDrive;
    }


}
