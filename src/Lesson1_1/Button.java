package Lesson1_1;

public class Button {

    //Поле - та информация, которую мы храним или используем в классе
    public String name;

    //Метод - как мы работаем с объектом. Что усеет объект
    public String pressButton (String prefix, Integer value) {
        return prefix + name + value;
    }

    public void NotifyIfPress () {
        System.out.println("Нажата клавиша " + name);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
